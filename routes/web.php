<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use function foo\func;

Route::get('/', function () {
    return view('layouts.master');
});

Auth::routes();


Route::group(['middleware' => 'auth'],function(){
    Route::get('/home/all', 'HomeController@index')->name('home');

    Route::get('/home/{id?}','ToDoListController@show')->name('list');

    Route::post('/create/todo','ToDoController@store')->name('add-todo');

    Route::get('/completed/{id}','ToDoController@complete')->name('completed');

    Route::post('/add/list','ToDoListController@addList')->name('addList');
});
