@extends('layouts.master')

@section('style')
    <style>
        .todo-list{
            list-style: none;
            float: right;
            height:45px;
        }
        .striked{
            text-decoration: line-through;
        }

        .todo-block-items{
            display: inline-block;
        }
        
        .todo-list-note{
            display: none;
        }

    </style>
@endsection

@section('content')
    <div class="container" style="max-width: 95%;margin:0;">
        <div class="row">
            <div class="col-md-10">
                <h3 class="page-title">{{ $todoList->name }}</h3>

                <div class="add-todo-section">
                    <form action="{{ route('add-todo') }}" method="POST">

                        {{ csrf_field() }}

                        <div class="form-group">
                            <label for="add-todo-form-title">ToDo:</label>
                            <input type="text" class="form-control" name="title" id="add-todo-form-title" placeholder="Add what you need to do..">
                        </div>

                        <div class="form-group" id="note-section-id" style="display: none;margin-top: 10px;">
                            <labe for="note">Note:</labe>
                            <textarea class="form-control" rows="5" id="note" style="resize: none;"
                                      placeholder="Add a note to your todo item" name="note"></textarea>
                        </div>

                        <input type="hidden" name="list_id" value="{{ $todoList->id }}">

                        <button class="btn btn-success"><i class="icon-settings"></i> Add to my list</button>

                        <button class="btn btn-success" type="button" id="addNote">Add Note</button>

                        @include('partials.form-errors')
                    </form>
                </div>
                <hr>

                    @if(count($myTodos) > 0)
                       <div id="todo-list-section">

                            @foreach($myTodos as $do)
                                <div class="todo-list-div">
                                    <div class="todo-block-items">
                                        <input type="checkbox" name="checkbox"
                                               onchange="window.location.href='{{ route('completed',$do->id) }}'">
                                        <li class="todo-list">{{ $do->title }}</li>

                                    </div>
                                    <p class="todo-list-note">{{ $do->note }}</p>
                                </div>
                            @endforeach

                       </div>
                    @else
                        <h3>Your list is empty!Add todo to list?</h3>
                    @endif
            </div>

        </div>
    </div>
@endsection


@section('script')
    <script>

        $("#addNote").click(function (e) {
            $("#note-section-id").toggle("slow");
            document.getElementById('addNote').value = "changed";
        });


        $("input[type=checkbox]").change(function(){
            $(this).next().addClass("striked").hide(300);
            $(this).toggle(300);
            $(this).parent().parent().hide(300);
        });
    </script>
@endsection