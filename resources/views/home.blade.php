@extends('layouts.master')

@section('content')
<div class="container" style="max-width: 95%;margin:0;">
    <div class="row">
        <div class="col-md-12">
            <h3 class="page-title">All todos</h3>


            <div class="add-todo-section">
                <form action="{{ route('add-todo') }}" method="POST">

                    {{ csrf_field() }}

                    <div class="form-group">
                        <label for="add-todo-form-title">ToDo:</label>
                        <input type="text" class="form-control" name="title" id="add-todo-form-title" placeholder="Add what you need to do..">
                    </div>

                    <div class="form-group" id="note-section-id" style="display: none;margin-top: 10px;">
                        <labe></labe>
                        <textarea class="form-control" rows="5" id="note" style="resize: none;"
                                  placeholder="Add a note to your todo item" name="note"></textarea>
                    </div>

                    <input type="hidden" name="list_id" value="1">

                    <button class="btn btn-success"><i class="icon-settings"></i> Add to my list</button>

                    <button class="btn btn-success" type="button" id="addNote">Add Note</button>

                    @include('partials.form-errors')
                </form>
            </div>
            <hr>

                @if(Auth::check())
                    @foreach(Auth::user()->todos as $todo)
                        <li>{{ $todo->title }}</li>
                    @endforeach
                @endif
        </div>
    </div>
</div>
@endsection


@section('script')
    <script>

        $("#addNote").click(function (e) {
            $("#note-section-id").toggle("slow");
            document.getElementById('addNote').value = "changed";
        });
    </script>
@endsection