<!-- BEGIN SIDEBAR -->
    <div class="page-sidebar-wrapper">
        
        <div class="page-sidebar navbar-collapse collapse">
            
            <ul class="page-sidebar-menu" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
                <li class="sidebar-toggler-wrapper">
                    
                </li>
                <li class="sidebar-search-wrapper">
                    <form class="sidebar-search " >
                        <a href="google" class="remove">
                            <i class="icon-close"></i>
                        </a>
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Search...">
                            <span class="input-group-btn">
                            <a href="javascript:;" class="btn submit"><i class="icon-magnifier"></i></a>
                            </span>
                        </div>
                    </form>
                </li>
                <li>
                    <a href="{{ route('home') }}">
                        <i class="fa fa-home"></i>
                        <span class="title">All</span>
                        <span class="arrow "></span>
                    </a>
                </li>
                @foreach($todolistItem as $item)
                    <li>
                        <a href="{{ route('list',$item->id) }}">
                            <i class="fa fa-th-list"></i>
                            <span class="title">{{ $item->name }}</span>
                            <span class="arrow "></span>
                        </a>
                    </li>
                @endforeach

                <li class="sidebar-search-wrapper">
                    <form class="sidebar-search "  method="POST" id="addListForm" action="{{ route('addList') }}">
                        {{ csrf_field() }}
                        <div class="input-group">
                            <input type="text" class="form-control" name="list_name" style="color:white;" placeholder="Add new list">
                            <span class="input-group-btn">
                            <button id="form-add-button" style="background-color: #364150;border:none;">
                                <i class="fa fa-plus" aria-hidden="true" style="color:white"></i>Add
                            </button>
                            </span>
                        </div>
                    </form>
                </li>
                              
            </ul>
            <!-- END SIDEBAR MENU -->
        </div>
    </div>
    <!-- END SIDEBAR -->

@section('script')
    <script>
        $("#form-add-button").click(function () {
            alert('do soemthing');
        });
    </script>
@endsection