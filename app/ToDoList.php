<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ToDoList extends Model
{
    protected $table = 'lists';

    public function todos()
    {
        return $this->hasMany('App\ToDo');
    }
}
