<?php

namespace App\Http\Controllers;

use function dd;
use Illuminate\Database\Eloquent\ModelNotFoundException as ModelNotFoundException;

use Illuminate\Http\Request;
use App\ToDoList;
use App\ToDo;

use function redirect;
use function var_dump;


class ToDoListController extends Controller
{
    public function show($id = null)
    {
        if (empty($id)) {
            return redirect()->home();
        } else {
            try {
                $todoList = ToDoList::findOrFail($id);

                $myTodos = Todo::where('list_id', $id)
                    ->where('is_completed', 0)
                    ->get();

                return view('todo.show')->
                with(['todoList' => $todoList, 'myTodos' => $myTodos]);

            } catch (ModelNotFoundException $e) {
//                No Data Found Using ID
//                uncomment dd for debug
//                dd($e);
                return redirect()->home();
            }

        }

    }

    public function addList(Request $request)
    {
        $this->validate($request,[
           'list_name' => 'required|max:190'
        ]);

        $list = new ToDoList;
        $list->name = $request->list_name;
        $list->uid  = rand();

        $list->save();

        return redirect()->back();
    }


}
