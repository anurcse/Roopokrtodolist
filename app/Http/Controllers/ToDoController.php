<?php

namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\User;
use App\ToDo;
use App\ToDoList;

use Auth;

class ToDoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function store(Request $request)
    {

        $this->validate($request,[
            'title' => 'required|max:255',
            'note'  => 'max:300',
            'list_id' => 'numeric'
        ]);

        $todo = new ToDo;

        $todo->title = $request->title;
        $todo->uid   =  rand();

        if(!empty($request->note))
        {
            $todo->note = $request->note;
        }

        if($request->list_id == 0 || empty($request->list_id))
        {
            $todo->list_id = 0;
        }else{
            $todo->list_id = $request->list_id;
        }

        $todo->user_id = Auth::id();

        $todo->save();

        if($request->list_id == 0 || empty($request->list_id))
        {
            return redirect()->route('list',1);

        }

        return redirect()->route('list',$request->list_id);

    }

    public function complete($id)
    {

        try{
            $toDo = ToDo::findOrfail($id);
            $toDo->is_completed = 1;
            $toDo->save();
            return redirect()->back();

        }catch (ModelNotFoundException $e)
        {
            dd($e);
        }
    }
}
