<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ToDo extends Model
{

    public function listName()
    {
        return $this->belongsTo('App\ToDoList');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
